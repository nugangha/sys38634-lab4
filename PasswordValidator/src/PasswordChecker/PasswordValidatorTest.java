package PasswordChecker;

import static org.junit.Assert.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.*;

import java.lang.annotation.Annotation;
import java.lang.reflect.Executable;
import java.lang.reflect.Field;

import javax.swing.text.AbstractDocument.Content;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class PasswordValidatorTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}
	@Test
	public void testPasswordLength() {
				
		if("Pat9gray".length() <= 8) {
			System.out.println("The password is incorrect " + "Pat9gray");
		}
		System.out.println("The name " + "Pat9gray" + " contains " + "Pat9gray".length() + "letters.");
		
		assertEquals(8, "Pat9gray".length());
		
		
	}
	@Test
	public void testPasswordLengthBoundaryIn() {
		if("Pat9gay".length() <= 8) {
			System.out.println("The password is incorrect " + "Pat9gray");
		}
		System.out.println("The name " + "Pat9gray" + " contains " + "Pat9gray".length() + "letters.");
		
		assertEquals(8, "Pat9gayr".length());
	}
	@Test
	public void testPasswordLengthBoundaryOut() {
		if("Pat9gr9ay".length() <= 8) {
			System.out.println("The password is incorrect " + "Pat9gray");
		}
		System.out.println("The name " + "Pat9gray" + " contains " + "Pat9gray".length() + "letters.");
		
		assertEquals(8, "Pat9gr9y".length());
	}
	@Test
	public void testPasswordDigits() {	
		
		boolean b; // a boolean value to compare the set of values
	      
	      int count =0;
	     
	        // Convert string to a char array.
	        // Loop over chars in the array.
	        for (char c : "P8tg9ry".toCharArray()) {
	            System.out.println(c);
	            
	            //check if character is digits
	            if(b = Character.isDigit(c)) {
	            	System.out.println("this is digit " + count++);
	            }
	        }
       assertEquals(2, count);
 
	}
	@Test
	public void testPasswordDigitsBoundaryIn() {

		boolean b; // a boolean value to compare the set of values
	      
	      int count =0;
	     
	        // Convert string to a char array.
	        // Loop over chars in the array.
	        for (char c : "P8tg9ry".toCharArray()) {
	            System.out.println(c);
	            
	            //check if character is digits
	            if(b = Character.isDigit(c)) {
	            	System.out.println("this is digit " + count++);
	            }
	        }
       assertEquals(2, count);
	}
	@Test
   public void testPasswordDigitsBoundaryOut() {

		boolean b; // a boolean value to compare the set of values
	      
	      int count =0;
	     
	        // Convert string to a char array.
	        // Loop over chars in the array.
	        for (char c : "P8tg9ry".toCharArray()) {
	            System.out.println(c);
	            
	            //check if character is digits
	            if(b = Character.isDigit(c)) {
	            	System.out.println("this is digit " + count++);
	            }
	        }
       assertEquals(2, count);
	}
	@Test
	public void testSetPasswordException(){
	     try {
	            PasswordValidator user = new PasswordValidator();
	              user.setPassword("Ghjkjalj");
	              
	      }catch (IllegalArgumentException ex){
	   
	              assertEquals("Password can't be blank", ex.getMessage());
	           }

	}
	@Test (expected = IllegalArgumentException.class)
	public void testSetPasswordExcept(){
	      
		PasswordValidator user = new PasswordValidator();
	     user.setPassword("pasdf");

	}

}
